describe 'titlecase', ->
  beforeEach(module('NamaraShared'))

  filter = null
  beforeEach inject ($filter)->
    filter = $filter('titlecase')

  describe 'titlecase', ->
    it 'should convert a single word to titlecase', ->
      expect(filter('hello')).toEqual('Hello')
    it 'should convert multiple words to titlecase', ->
      expect(filter('hello world')).toEqual('Hello World')
    it 'should not fail with a single letter', ->
      expect(filter('h')).toEqual('H')
    it 'should not fail with a lot of spaces', ->
      expect(filter('hello  world')).toEqual('Hello  World')
