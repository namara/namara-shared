describe 'nmField', ->
  beforeEach(module('NamaraShared'))

  element = null
  scope = null
  beforeEach inject ($compile, $rootScope)->
    $scope = $rootScope.$new()
    element = angular.element('<nm-field label="Foo bar"><input type="text"></nm-field>')
    element = $compile(element)($scope)
    $scope.$digest()
    scope = element.isolateScope()

  describe 'fieldName', ->
    it 'should set field name to value of label', ->
      expect(scope.fieldName).toEqual('foo-bar')
