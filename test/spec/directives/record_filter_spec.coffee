describe 'nmRecordFilter', ->
  beforeEach(module('NamaraShared'))

  element = null
  scope = null
  beforeEach inject ($compile, $rootScope)->
    scope = $rootScope.$new()
    scope.filterModel = {}
    element = angular.element('<nm-record-filter record-filter-column="foo"\n                    record-filter-model="filterModel"\n                    record-filter-values="[\'bar\', \'baz\']"\n                    record-filter-default-value="baz"></nm-record-filter>')
    element = $compile(element)(scope)
    scope.$digest()
    scope = element.isolateScope()

  it 'should set values', ->
    expect(scope.values).toEqual(['bar', 'baz'])

  it 'should set column', ->
    expect(scope.column).toEqual('foo')

  it 'should set model', ->
    expect(scope.model).toEqual({foo: 'baz'})

  it 'should set default value', ->
    expect(scope.default).toEqual('baz')

  describe '#filterRecords', ->

    it 'should set filter by column', ->
      scope.filterRecords('bar')
      expect(scope.model.foo).toEqual('bar')

    it 'should clear filter if no value is provided', ->
      scope.filterRecords('bar')
      scope.filterRecords()
      expect(scope.model.foo).toBeUndefined()
