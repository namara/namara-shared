describe 'nmAutoSelect', ->
  beforeEach(module('NamaraShared'))

  element = null
  scope = null
  $window = null
  beforeEach inject ($compile, $rootScope, _$window_)->
    $window = _$window_
    $scope = $rootScope.$new()
    $scope.model = {text: 'foo'}
    element = angular.element('<input type="text" ng-model="model.text" nm-auto-select>')
    element = $compile(element)($scope)
    $scope.$digest()
    scope = element.isolateScope()

  describe 'clicking the element', ->

    it 'will call select on the element', ->
      spyOn(element[0], 'select').andCallThrough()
      element[0].click()
      expect(element[0].select).toHaveBeenCalled()
