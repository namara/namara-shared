describe 'crudEdit', ->
  beforeEach(module('NamaraShared', ($provide)->
    ->
      ngDialog = jasmine.createSpyObj('ngDialog', ['open'])
      $provide.value('ngDialog', ngDialog)
  ))

  element = null
  scope = null
  $q = null
  $scope = null
  beforeEach inject ($compile, _$rootScope_, _$q_)->
    $scope = _$rootScope_
    $q = _$q_
    $scope.model = {}
    $scope.onSave = jasmine.createSpy('onSave')
    $scope.onError = jasmine.createSpy('onError')
    element = angular.element('<form crud-edit="model" on-save="onSave()" on-error="onError()">\n  <input type="text" name="title" ng-model="model.title" required>\n  <input type="text" name="content" ng-model="model.content">\n</form>')
    element = $compile(element)($scope)
    $scope.$apply()
    scope = element.scope()

  describe '#save', ->
    defer = null
    beforeEach ->
      defer = $q.defer()
      scope.model.save = jasmine.createSpy().andReturn(defer.promise)

    it 'should be defined', ->
      expect(scope.save).toBeDefined()

    describe 'with invalid record', ->
      beforeEach ->
        spyOn(scope, 'canSave').andReturn(false)

      it 'should not call record#save', ->
        scope.save()
        expect(scope.model.save).not.toHaveBeenCalled()

      it 'should call #onError', ->
        scope.save()
        expect(scope.onError).toHaveBeenCalled()

    describe 'with valid record', ->
      beforeEach ->
        spyOn(scope, 'canSave').andReturn(true)

      it 'should call record#save', ->
        scope.save()
        expect(scope.model.save).toHaveBeenCalled()

      it 'should call #onSave', ->
        $scope.$apply ->
          scope.save()
          defer.resolve()
        expect(scope.onSave).toHaveBeenCalled()

  describe '#canSave', ->
    it 'should be defined', ->
      expect(scope.canSave).toBeDefined()

    it 'should respond with true form is true', ->
      $scope.$apply ->
        scope.model.title = 'foo'
      expect(scope.canSave()).toBe(true)
