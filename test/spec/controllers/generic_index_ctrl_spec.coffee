describe 'GenericIndexCtrl', ->
  $scope = null
  beforeEach(module('NamaraShared'))
  beforeEach inject ($rootScope)->
    $scope = $rootScope.$new()

  ctrl = null
  beforeEach inject ($controller)->
    ctrl = $controller 'GenericIndexCtrl', {
      $scope: $scope,
      records: []
    }

  it 'should assign records', ->
    expect($scope.records).toBeDefined()

  it 'should assign record filter', ->
    expect($scope.recordsFilter).toBeDefined()

  describe '#deleteRecord', ->

    it 'should be defined', ->
      expect($scope.deleteRecord).toBeDefined()

    it 'should call destroy on record', ->
      spyOn(MockRecord.prototype, 'delete')
      $scope.deleteRecord(new MockRecord())
      expect(MockRecord.prototype.delete).toHaveBeenCalled()
