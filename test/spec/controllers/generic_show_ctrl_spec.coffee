describe 'GenericShowCtrl', ->
  $scope = null
  beforeEach(module('NamaraShared'))
  beforeEach inject ($rootScope)->
    $scope = $rootScope.$new()

  ctrl = null
  beforeEach inject ($controller)->

    ctrl = $controller 'GenericShowCtrl', {
      $scope: $scope,
      record: new MockRecord()
    }

  it 'should assign record', ->
    expect($scope.record).toBeDefined()
