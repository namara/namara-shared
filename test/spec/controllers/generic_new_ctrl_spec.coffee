describe 'GenericNewCtrl', ->
  $scope = null
  beforeEach(module('NamaraShared', ($provide)->
    ->
      $provide.value('MockRecord', MockRecord)
  ))
  beforeEach inject ($rootScope)->
    $scope = $rootScope.$new()

  ctrl = null
  beforeEach inject ($controller)->

    ctrl = $controller 'GenericNewCtrl', {
      $scope: $scope,
      $route: {
        current:
          $$route:
            recordType: 'MockRecord'
      }
    }

  it 'should assign record', ->
    expect($scope.record).toBeDefined()
