angular.module('RecursionHelper', []).factory('RecursionHelper', ['$compile', function($compile){
  return {
    /**
     * Manually compiles the element, fixing the recursion loop.
     * @param element
     * @param [link] A post-link function, or an object with function(s) registered via pre and post properties.
     * @returns An object containing the linking functions.
     */
    compile: function(element, link){
      // Normalize the link parameter
      if(angular.isFunction(link)){
        link = { post: link };
      }

      // Break the recursion loop by removing the contents
      var contents = element.contents().remove();
      var compiledContents;
      return {
        pre: (link && link.pre) ? link.pre : null,
        /**
         * Compiles and re-adds the contents
         */
        post: function(scope, element){
          // Compile the contents
          if(!compiledContents){
            compiledContents = $compile(contents);
          }
          // Re-add the compiled contents to the element
          compiledContents(scope, function(clone){
            element.append(clone);
          });

          // Call the post-linking function, if any
          if(link && link.post){
            link.post.apply(null, arguments);
          }
        }
      };
    }
  };
}]);
;
(function() {
  angular.module('NamaraShared', ['ngDialog', 'RecursionHelper']);

  angular.module('NamaraShared').controller('GenericIndexCtrl', function($scope, records) {
    $scope.records = records;
    $scope.recordsFilter = {};
    return $scope.deleteRecord = function(record) {
      record["delete"]();
      return $scope.records.splice($scope.records.indexOf(record), 1);
    };
  });

  angular.module('NamaraShared').controller('GenericNewCtrl', function($scope, $route, $injector) {
    var recordClass;
    recordClass = $injector.get($route.current.$$route.recordType);
    return $scope.record = new recordClass();
  });

  angular.module('NamaraShared').controller('GenericShowCtrl', function($scope, record) {
    return $scope.record = record;
  });

  angular.module('NamaraShared').directive('nmAutoSelect', function() {
    return {
      restrict: 'A',
      link: function(scope, elem) {
        return elem.bind('click', function() {
          return this.select();
        });
      }
    };
  });

  angular.module('NamaraShared').directive('crudButtons', function() {
    return {
      restrict: 'E',
      template: '<div class="row form-field">\n  <div class="columns">\n    <input type="submit" class="button" ng-disabled="!canSave()" ng-click="save()" value="Save">\n  </div>\n</div>\n'
    };
  });

  angular.module('NamaraShared').directive('crudEdit', function($parse, NamaraDialog) {
    return {
      restrict: 'A',
      require: '^form',
      scope: true,
      link: function(scope, elem, attrs, form) {
        var beforeSave, getter, onError, onSave, record;
        getter = $parse(attrs.crudEdit);
        record = getter(scope);
        beforeSave = $parse(attrs.beforeSave);
        onSave = $parse(attrs.onSave);
        onError = $parse(attrs.onError);
        scope.save = function() {
          if (scope.canSave()) {
            beforeSave(scope);
            return record.save().then(function() {
              onSave(scope);
              return NamaraDialog.open('Success', 'The resource has been saved successfully.');
            });
          } else {
            return onError(scope);
          }
        };
        return scope.canSave = function() {
          return form.$valid;
        };
      }
    };
  });

  angular.module('NamaraShared').directive('nmField', function() {
    return {
      restrict: 'E',
      transclude: true,
      replace: true,
      template: '<div class="row form-field">\n  <div ng-class="nestedField ? \'large-12\' : \'large-5\'" class="columns">\n    <label>{{label}}</label>\n    <div class="input" ng-class="fieldName" ng-transclude></div>\n  </div>\n</div>\n',
      scope: {
        label: '@'
      },
      controller: function($scope) {
        return $scope.fieldName = $scope.label.toLowerCase().replace(' ', '-');
      },
      link: function(scope, elem, attrs) {
        if (attrs.nestedField != null) {
          return scope.nestedField = true;
        }
      }
    };
  });

  angular.module('NamaraShared').directive('nmRecordFilter', function() {
    return {
      restrict: 'E',
      replace: true,
      scope: {
        column: '@recordFilterColumn',
        values: '=recordFilterValues',
        model: '=recordFilterModel',
        "default": '@recordFilterDefaultValue'
      },
      template: '<dl class="sub-nav">\n  <dt>Filter:</dt>\n  <dd ng-class="isActive() ? \'active\' : \'\'">\n    <a href="" ng-click="filterRecords()">All</a>\n  </dd>\n  <dd ng-repeat="value in values" ng-class="isActive(value) ? \'active\' : \'\'">\n    <a href="" ng-click="filterRecords(value)">{{value | titlecase}}</a>\n  </dd>\n</dl>\n',
      controller: function($scope) {
        $scope.model[$scope.column] = $scope["default"];
        $scope.filterRecords = function(value) {
          if (value == null) {
            value = null;
          }
          if (value === null) {
            return delete $scope.model[$scope.column];
          } else {
            return $scope.model[$scope.column] = value;
          }
        };
        return $scope.isActive = function(value) {
          if (value == null) {
            value = null;
          }
          if (value === null) {
            return $scope.model[$scope.column] == null;
          } else {
            return $scope.model[$scope.column] === value;
          }
        };
      }
    };
  });

  angular.module('NamaraShared').directive('nmThrottleInput', function() {
    return {
      restrict: 'A',
      link: function(scope, elem, attrs) {
        var delay, throttledKeyDown;
        delay = attrs.nmDelay || 500;
        throttledKeyDown = _.throttle(function() {
          return scope.$eval(attrs.nmThrottleInput);
        }, delay);
        return elem.bind('keydown', throttledKeyDown);
      }
    };
  });

  angular.module('NamaraShared').filter('titlecase', function() {
    return function(input) {
      var i, word, words, _i, _len;
      words = input.toLowerCase().replace(/_|-/g, ' ').split(' ');
      for (i = _i = 0, _len = words.length; _i < _len; i = ++_i) {
        word = words[i];
        words[i] = word.charAt(0).toUpperCase() + word.slice(1);
      }
      return words.join(' ');
    };
  });

  angular.module('NamaraShared').factory('GenericResolver', function($q, $route, $location, $injector) {
    var GenericResolver;
    return GenericResolver = (function() {
      function GenericResolver(recordClass) {
        this.recordClass = $injector.get(recordClass);
        this.defer = $q.defer();
      }

      GenericResolver.prototype.all = function(params) {
        if (params == null) {
          params = {};
        }
        this.recordClass.query(params).then((function(_this) {
          return function(records) {
            return _this.defer.resolve(records);
          };
        })(this));
        return this.defer.promise;
      };

      GenericResolver.prototype.get = function(recordId) {
        if (recordId == null) {
          recordId = this.getRecordId();
        }
        this.recordClass.get(recordId).then((function(_this) {
          return function(record) {
            return _this.defer.resolve(record);
          };
        })(this));
        return this.defer.promise;
      };

      GenericResolver.prototype.search = function() {
        return this.all({
          query: $location.search().query
        });
      };

      GenericResolver.prototype.getRecordId = function() {
        return $route.current.params.recordId;
      };

      return GenericResolver;

    })();
  });

  angular.module('NamaraShared').factory('NamaraDialog', function(ngDialog, $rootScope) {
    return {
      open: function(title, message, type) {
        var scope;
        if (type == null) {
          type = 'info';
        }
        scope = $rootScope.$new();
        scope.title = title;
        scope.message = message;
        return ngDialog.open({
          template: '<div class="ngdialog-message">\n  <h4>{{title}}</h4>\n  <p>{{message}}</p>\n</div>\n',
          scope: scope,
          plain: true
        });
      }
    };
  });

}).call(this);
