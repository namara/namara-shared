module.exports = function (grunt) {
  grunt.loadNpmTasks('grunt-contrib-coffee');
  grunt.loadNpmTasks('grunt-contrib-sass');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-karma');

  grunt.initConfig({
    coffee: {
      compileJoined: {
        options: {
          join: true
        },
        files: {
          'dist/namara_shared.js': 'src/scripts/**/*.coffee'
        }
      }
    },
    sass: {
      dist: {
        options: {
          style: 'expanded'
        },
        files: {
          'dist/namara_shared.css': 'src/styles/namara_common.scss'
        }
      }
    },
    concat: {
      options: {
        separator: ";\n"
      },
      dist: {
        src: ['src/scripts/**/*.js', 'dist/namara_shared.js'],
        dest: 'dist/namara_shared.js'
      }
    },
    watch: {
      scripts: {
        files: 'src/scripts/**/*.coffee',
        tasks: ['coffee', 'karma']
      },
      styles: {
        files: 'src/styles/**/*.scss',
        tasks: ['sass']
      },
      test: {
        files: ['test/spec/**/*.coffee'],
        tasks: ['newer:coffee:test', 'karma']
      }
    },
    clean: {
      dist: ['dist']
    },
    karma: {
      unit: {
        configFile: 'test/karma.conf.js',
        singleRun: true,
        background: false
      }
    }
  });

  grunt.registerTask('default', function () {
    grunt.task.run([
      'clean:dist',
      'coffee',
      'sass',
      'concat',
      'karma'
    ]);
  });
};
