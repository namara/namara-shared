angular.module('NamaraShared')
  .filter 'titlecase', ->
    (input)->
      words = input.toLowerCase().replace(/_|-/g,' ').split(' ')
      for word,i in words
        words[i] = word.charAt(0).toUpperCase() + word.slice(1)

      words.join(' ')
