angular.module('NamaraShared').directive 'crudEdit', ($parse, NamaraDialog)->
  return {
    restrict: 'A'
    require: '^form'
    scope: true
    link: (scope, elem, attrs, form)->
      getter = $parse(attrs.crudEdit)
      record = getter(scope)

      beforeSave = $parse(attrs.beforeSave)
      onSave = $parse(attrs.onSave)
      onError = $parse(attrs.onError)
      scope.save = ->
        if scope.canSave()
          beforeSave(scope)
          record.save().then ->
            onSave(scope)
            NamaraDialog.open('Success', 'The resource has been saved successfully.')
        else
          onError(scope)

      scope.canSave = ->
        form.$valid

  }
