angular.module('NamaraShared').directive 'crudButtons', ->
  return {
    restrict: 'E'
    template: '<div class="row form-field">\n  <div class="columns">\n    <input type="submit" class="button" ng-disabled="!canSave()" ng-click="save()" value="Save">\n  </div>\n</div>\n'
  }
