angular.module('NamaraShared')
  .directive 'nmThrottleInput', ->
    return {
      restrict: 'A'
      link: (scope, elem, attrs)->
        delay = attrs.nmDelay || 500

        throttledKeyDown = _.throttle ->
          scope.$eval(attrs.nmThrottleInput)
        , delay

        elem.bind('keydown', throttledKeyDown)
    }
