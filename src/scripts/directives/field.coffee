angular.module('NamaraShared')
  .directive 'nmField', ->
    return {
      restrict: 'E',
      transclude: true,
      replace: true,
      template: '<div class="row form-field">\n  <div ng-class="nestedField ? \'large-12\' : \'large-5\'" class="columns">\n    <label>{{label}}</label>\n    <div class="input" ng-class="fieldName" ng-transclude></div>\n  </div>\n</div>\n',
      scope: {
        label: '@'
      },
      controller: ($scope)->
        $scope.fieldName = $scope.label.toLowerCase().replace(' ', '-')
      link: (scope, elem, attrs)->
        scope.nestedField = true if attrs.nestedField?
    }
