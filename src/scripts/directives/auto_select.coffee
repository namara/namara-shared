angular.module('NamaraShared')
  .directive 'nmAutoSelect', ->
    return {
      restrict: 'A'
      link: (scope, elem)->
        elem.bind 'click', ->
          this.select()
    }
