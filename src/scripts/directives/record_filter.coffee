angular.module('NamaraShared')
  .directive 'nmRecordFilter', ->
    return {
      restrict: 'E'
      replace: true
      scope: {
        column: '@recordFilterColumn'
        values: '=recordFilterValues'
        model: '=recordFilterModel'
        default: '@recordFilterDefaultValue'
      }
      template: '<dl class="sub-nav">\n  <dt>Filter:</dt>\n  <dd ng-class="isActive() ? \'active\' : \'\'">\n    <a href="" ng-click="filterRecords()">All</a>\n  </dd>\n  <dd ng-repeat="value in values" ng-class="isActive(value) ? \'active\' : \'\'">\n    <a href="" ng-click="filterRecords(value)">{{value | titlecase}}</a>\n  </dd>\n</dl>\n'
      controller: ($scope)->
        $scope.model[$scope.column] = $scope.default

        $scope.filterRecords = (value = null)->
          if value == null
            delete $scope.model[$scope.column]
          else
            $scope.model[$scope.column] = value

        $scope.isActive = (value = null)->
          if value == null
            !$scope.model[$scope.column]?
          else
            $scope.model[$scope.column] == value
    }
