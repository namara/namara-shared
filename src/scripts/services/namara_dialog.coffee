angular.module('NamaraShared')
  .factory 'NamaraDialog', (ngDialog, $rootScope)->
    return {
      open: (title, message, type = 'info')->
        scope = $rootScope.$new()
        scope.title = title
        scope.message = message
        ngDialog.open
          template: '<div class="ngdialog-message">\n  <h4>{{title}}</h4>\n  <p>{{message}}</p>\n</div>\n'
          scope: scope
          plain: true
    }
