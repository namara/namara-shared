angular.module('NamaraShared').controller 'GenericIndexCtrl', ($scope, records)->
  $scope.records = records
  $scope.recordsFilter = {}

  $scope.deleteRecord = (record)->
    record.delete()
    $scope.records.splice($scope.records.indexOf(record), 1)
