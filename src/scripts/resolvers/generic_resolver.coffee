angular.module('NamaraShared')
  .factory 'GenericResolver', ($q, $route, $location, $injector)->
    class GenericResolver

      constructor: (recordClass)->
        @recordClass = $injector.get(recordClass)
        @defer = $q.defer()

      all: (params = {})->
        @recordClass.query(params).then (records)=>
          @defer.resolve(records)

        return @defer.promise

      get: (recordId = @getRecordId())->
        @recordClass.get(recordId).then (record)=>
          @defer.resolve(record)

        return @defer.promise

      search: ->
        @all({query: $location.search().query})

      getRecordId: ->
        $route.current.params.recordId
